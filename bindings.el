;; Install evil-leader package
(use-package evil-leader
  :ensure t
  :init
  (global-evil-leader-mode)
  (setq evil-leader/in-all-states t)
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key "." 'find-file) ;; open a file
  (evil-leader/set-key "op" 'treemacs) ;; open file tree (treemacs)
  (evil-leader/set-key "hr" (lambda () (interactive) (load-file "~/.emacs.d/init.el"))) ;; reload my config
  (evil-leader/set-key "g" 'magit-status :description "Git status")
  ;; Add more keybindings here
  )
