;; Generated package description from setup.el  -*- no-byte-compile: t -*-
(define-package "setup" "1.3.2" "Helpful Configuration Macro" '((emacs "26.1")) :commit "28926bd11eef6118f4e169d10c1c36b8c4e545ae" :authors '(("Philip Kaludercic" . "philipk@posteo.net")) :maintainer '("Philip Kaludercic" . "~pkal/public-inbox@lists.sr.ht") :keywords '("lisp" "local") :url "https://git.sr.ht/~pkal/setup")
