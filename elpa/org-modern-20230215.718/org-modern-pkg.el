;;; Generated package description from org-modern.el  -*- no-byte-compile: t -*-
(define-package "org-modern" "20230215.718" "Modern looks for Org" '((emacs "27.1")) :commit "42fdbd2d27f5a8652593577a200eeea96555d018" :authors '(("Daniel Mendler" . "mail@daniel-mendler.de")) :maintainer '("Daniel Mendler" . "mail@daniel-mendler.de") :url "https://github.com/minad/org-modern")
