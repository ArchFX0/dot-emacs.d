#+title: Change log of the Standard Themes
#+author: Protesilaos Stavrou
#+email: info@protesilaos.com
#+options: ':nil toc:nil num:nil author:nil email:nil

This document contains the release notes for each tagged commit on the
project's main git repository: <https://git.sr.ht/~protesilaos/standard-themes>.

The newest release is at the top.  For further details, please consult
the manual: <https://protesilaos.com/emacs/standard-themes>.

* Version 1.2.0 on 2023-02-16

** Support for palette overrides

It is now possible to override the palette of each Standard theme.
This is the same feature that I implemented for the ~modus-themes~,
except it is a bit more limited in scope (the Modus themes are
maximalist due to their accessibility target).

Overrides allow the user to tweak the presentation of either or both
themes, such as to change the colour value of individual entries
and/or remap how named colours are applied to semantic code
constructs.

For example, the user can change what the exact value of ~blue-warmer~
is and then, say, make comments use a shade of green instead of red.

There are three user options to this end:

+ ~standard-themes-common-palette-overrides~ which covers both themes.
+ ~standard-dark-palette-overrides~ which concerns the dark theme.
+ ~standard-light-palette-overrides~ which is for the light theme.

The theme-specific overrides take precedence over the "common" ones.

The theme's palette with named colors can be previewed with the
commands ~standard-themes-preview-colors~ and
~standard-themes-preview-colors-current~.  When called with a
universal prefix argument (=C-u= with default key bindings) these
commands produce a preview of the semantic colour mappings (e.g. what
colour applies to level 2 headings).

Use the preview as a reference to find entries to override.  And
consult the manual for the technicalities.

Thanks to Clemens Radermacher for fixing a mistake I made in the code
that produces the palette previews.

** Added the function ~standard-themes-get-color-value~

It returns the colour value of named =COLOR= for the current Standard
theme.

=COLOR= is a symbol that represents a named colour entry in the
palette.

If the value is the name of another colour entry in the palette (so a
mapping), recur until you find the underlying colour value.

With optional =OVERRIDES= as a non-nil value, account for palette
overrides.  Else use the default palette.

With optional =THEME= as a symbol among ~standard-themes-items~, use
the palette of that item.  Else use the current Standard theme.

If =COLOR= is not present in the palette, return the ~unspecified~
symbol, which is safe when used as a face attribute's value.

The manual provides this information and also links to relevant
entries.  The example it uses, with the ~standard-light~ as current:

#+begin_src emacs-lisp
;; Here we show the recursion of palette mappings.  In general, it is
;; better for the user to specify named colors to avoid possible
;; confusion with their configuration, though those still work as
;; expected.
(setq standard-themes-common-palette-overrides
      '((cursor red)
        (prompt cursor)
        (variable prompt)))

;; Ignore the overrides and get the original value.
(standard-themes-get-color-value 'variable)
;; => "#a0522d"

;; Read from the overrides and deal with any recursion to find the
;; underlying value.
(standard-themes-get-color-value 'variable :overrides)
;; => "#b3303a"
#+end_src

** New user option ~standard-themes-disable-other-themes~

This user option is set to ~t~ by default.  This means that loading a
Standard theme with the command ~standard-themes-toggle~ or the
functions ~standard-theme-load-dark~,  ~standard-theme-load-light~
will disable all ~custom-enabled-themes~.

When the value of this user option is nil, themes are loaded without
disabling other entries outside their family.  This retains the
original (and in my opinion bad for most users) behaviour of Emacs
where it blithely blends multiple enabled themes.

I consider the blending a bad default because it undoes the work of
the designer and often leads to highly inaccessible and unpredictable
combinations.  Sure, experts can blend themes which is an argument in
favour of making that behaviour opt-in.

** Other changes

+ Refined the ~standard-dark~ theme's ~bg-hl-line~ background.  This
  makes it easier to read the underlying text of the currently
  highlighted line in hl-line-mode.

  Thanks to Manuel Uberti for the feedback on the mailing list:
  <https://lists.sr.ht/~protesilaos/standard-themes/%3C6e218fc0-f2dc-e03f-4e42-da0cbf9bd79b%40inventati.org%3E>.

+ Clarified some statements in the documentation about the palette
  overrides.  Thanks to Tassilo Horn for the feedback on the mailing
  list: <https://lists.sr.ht/~protesilaos/standard-themes/%3C87cz8bjrwz.fsf%40gnu.org%3E>.

** Acknowledgement

Thanks to Fritz Grabo who provided feedback via a private channel.
With it, I was able to better understand the underlying patterns of
the out-of-the-box Emacs faces and thus design the ~standard-themes~
accordingly.  This information is shared with permission.

As a reminder, the Standard themes are an interpretation of the
default Emacs faces (which technically are not a "theme").  I have
expanded the effective palette with harmonious entries, made mappings
that are consistent with the patterns found in some base faces, and
extended support for lots of packages.  At first sight, the Standard
themes look like what you get with an unconfigured Emacs.  Though make
no mistake: they are far more detail-oriented.

* Version 1.1.0 on 2022-12-06
:PROPERTIES:
:CUSTOM_ID: h:f7a5799c-279c-4dfb-96ae-3eba58ee582e
:END:

** The ~standard-themes-headings~ now covers the Org agenda
:PROPERTIES:
:CUSTOM_ID: h:d7fa8d0c-b49c-447a-a24c-4dc18c6d755b
:END:

The user option ~standard-themes-headings~ lets the user control the
height, weight, and use of proportionately spaced fonts
(~variable-pitch~) on a per-heading basis.  Different combinations are
possible, as explained in the option's doc string on the corresponding
entry in the manual.

I have now made it possible to specify the =agenda-date= and
=agenda-structure= keys.  Both pertain to the Org agenda.  The former
applies to date headings, while the latter styles the headings of each
"block" of content.  In a generic agenda buffer, that block heading is
the first line which reads =Week-agenda (W49):= or something like
that, though we find such headings in more places as well.

Here is a sample with various stylistic variants per heading:

#+begin_src emacs-lisp
(setq standard-themes-headings ; read the manual's entry or the doc string
      '((0 . (variable-pitch light 1.9))
        (1 . (variable-pitch light 1.8))
        (2 . (semilight 1.7))
        (3 . (semilight 1.6))
        (4 . (1.5)) ; absence of weight means "regular"
        (5 . (1.4))
        (6 . (bold 1.3))
        (7 . (bold 1.2))
        (agenda-date . (semilight 1.5))
        (agenda-structure . (variable-pitch light 1.9))
        (t . (variable-pitch 1.1))))
#+end_src

Note that Org re-uses heading levels past 8.  This means that level 9
will look the same as level 1.  This is not the theme's doing.  Check
the user options ~org-level-faces~, ~org-n-level-faces~ for ways to
change this.

** =M-x theme-choose-variant= works as expected
:PROPERTIES:
:CUSTOM_ID: h:9c95de58-9d0c-44dd-bc24-63ce7691806e
:END:

Users of Emacs 29 have access to the command ~theme-choose-variant~:
it toggles between two themes of the same family.  If the family
has more members, it uses minibuffer completion instead.

I registered the appropriate theme properties to make this work as
intended.  However, it is still possible to use the command
~standard-themes-toggle~.

** Stylistic refinements
:PROPERTIES:
:CUSTOM_ID: h:33e6d85e-42cc-4a30-87d5-47d9b81cc769
:END:

+ Simplified the Magit blame faces to avoid exaggerations.
+ Revised the colours of day headings in the =M-x calendar= buffer.
  Weekends stand out, as is the case with physical calendars and many
  established apps.
+ Made the ~edmacro-label~ face stand out in its context.  Otherwise
  it was difficult to spot.  This is for Emacs 29 and applies to
  headings in the keyboard macro editing buffer (e.g. with =C-x C-k
  C-e= (~kmacro-edit-macro-repeat~)).
+ Added support for the =powerline= package.
