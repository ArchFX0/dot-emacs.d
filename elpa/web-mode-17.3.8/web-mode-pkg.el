;; Generated package description from web-mode.el  -*- no-byte-compile: t -*-
(define-package "web-mode" "17.3.8" "major mode for editing web templates" '((emacs "23.1")) :commit "cb18eeb7d406bf55f1f0f77bf0c2425a72e4042b" :authors '(("François-Xavier Bois")) :maintainer '("François-Xavier Bois" . "fxbois@gmail.com") :keywords '("languages") :url "https://web-mode.org")
