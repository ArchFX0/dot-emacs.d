(define-package "yasnippet-snippets" "20230216.942" "Collection of yasnippet snippets"
  '((yasnippet "0.8.0"))
  :commit "0235f2b3f2d011e2875b4bc8cc6ec17d867a8fb6" :authors
  '(("Andrea Crotti" . "andrea.crotti.0@gmail.com"))
  :maintainer
  '("Andrea Crotti" . "andrea.crotti.0@gmail.com")
  :keywords
  '("snippets")
  :url "https://github.com/AndreaCrotti/yasnippet-snippets")
;; Local Variables:
;; no-byte-compile: t
;; End:
