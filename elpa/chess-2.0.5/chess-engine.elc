;ELC   
;;; Compiled
;;; in Emacs version 30.0.50
;;; with all optimizations.



(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\305\306\307\310\311\312%\207" [require chess-algebraic chess-fen chess-pgn chess-module custom-declare-group chess-engine nil "Code for reading movements and other commands from an engine." :group chess] 6)
(defvar chess-engine-regexp-alist nil)
(defvar chess-engine-response-handler nil)
(defvar chess-engine-current-marker nil)
(defvar chess-engine-pending-offer nil)
(defvar chess-engine-pending-arg nil)
(defvar chess-engine-opponent-name nil)
(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\300\306!\207" [make-variable-buffer-local chess-engine-regexp-alist chess-engine-response-handler chess-engine-current-marker chess-engine-pending-offer chess-engine-pending-arg chess-engine-opponent-name] 2)
(defvar chess-engine-process nil)
(defvar chess-engine-last-pos nil)
(defvar chess-engine-working nil)
(defvar chess-engine-handling-event nil)
(byte-code "\300\301!\210\300\302!\210\300\303!\207" [make-variable-buffer-local chess-engine-process chess-engine-last-pos chess-engine-working] 2)
(defvar chess-engine-inhibit-auto-pass nil)
(chess-message-catalog 'english '((invalid-fen . "Received invalid FEN string: %s") (invalid-pgn . "Received invalid PGN text") (now-black . "Your opponent played the first move, you are now black") (move-passed . "Your opponent has passed the move to you") (want-to-play . "Do you wish to play a chess game against %s? ") (want-to-play-a . "Do you wish to play a chess game against an anonymous opponent? ") (opp-quit . "Your opponent has quit playing") (opp-resigned . "Your opponent has resigned") (opp-draw . "Your opponent offers a draw, accept? ") (opp-abort . "Your opponent wants to abort this game, accept? ") (opp-undo . "Your opponent wants to take back %d moves, accept? ") (opp-ready . "Your opponent, %s, is now ready to play") (opp-ready-a . "Your opponent is ready to play; pass or make your move") (opp-draw-acc . "Your draw offer was accepted") (opp-abort-acc . "Your offer to abort was accepted") (opp-undo-acc . "Request to undo %d moves was accepted") (opp-draw-dec . "Your draw offer was declined") (opp-abort-dec . "Your offer to abort was declined") (opp-undo-dec . "Your request to undo %d moves was decline") (opp-draw-ret . "Your opponent has retracted their draw offer") (opp-abort-ret . "Your opponent has retracted their offer to abort") (opp-undo-ret . "Your opponent has retracted their request to undo %d moves") (opp-illegal . "Your opponent states your last command was illegal") (opp-call-flag . "Your flag fell, and your opponent has called time") (opp-flag-fell . "Your opponent has forfeited the game on time") (failed-start . "Failed to start chess engine process")))#@163 Convert algebraic move to a ply in reference to the engine position.
If conversion fails, this function fired an 'illegal event.

(fn MOVE &optional TRUST-CHECK)
(defalias 'chess-engine-convert-algebraic #[513 "\300\301\302!#\206 \303\302\304\"\207" [chess-algebraic-to-ply chess-engine-position nil chess-engine-command illegal] 6 (#$ . 2832)])
(byte-code "\300\301\302\303#\304\301\305\306#\207" [function-put chess-engine-convert-algebraic speed -1 put byte-optimizer byte-compile-inline-expand] 5)#@12 

(fn FEN)
(defalias 'chess-engine-convert-fen #[257 "\300!\206 \301C\302\303\304#!\266\305\207" [chess-fen-to-pos invalid-fen message apply chess-string nil] 8 (#$ . 3342)])
(byte-code "\300\301\302\303#\304\301\305\306#\207" [function-put chess-engine-convert-fen speed -1 put byte-optimizer byte-compile-inline-expand] 5)#@12 

(fn PGN)
(defalias 'chess-engine-convert-pgn #[257 "\300!\206 \301\302\303\304\305#!\266\302\207" [chess-pgn-to-game invalid-pgn nil message apply chess-string] 8 (#$ . 3678)])
(byte-code "\300\301\302\303#\304\301\305\306#\207" [function-put chess-engine-convert-pgn speed -1 put byte-optimizer byte-compile-inline-expand] 5)#@57 Default engine response handler.

(fn EVENT &rest ARGS)
(defalias 'chess-engine-default-handler #[385 "\306\307!\310\267\202_\311@\205\362 \312\313\"\205\362 	\204y \312\314\"\203y \211\211\3158\266\202GS\316U\203y \317\320\n#\210\317\321#\210\322\307\323\324\325#!\266\211\326C\f?\205j \307@\211\203i \211@\324@A$\262\210A\202R \210\266\312\314\"\203y \327\314\307#\210\330@\"\210\211\331\3158\332\"@\211\205\346 \211\333\334\335\336\337\340\341&\206\344 \211\211\211:\205\256 \211@\342!\205\254 \211G\343U\262\262\204\273 \344\345\346\347E\"\210@\211\342!\205\307 \211G\343U\262\204\324 \344\345\350\351E\"\210\211\352H\262\211\205\342 \333\353\354#\262\262\266\202\203\361 \327\313\307#\210\311)\207\312\313\"\205`\355\307\323\324\325#!\311\207\312\313\"\203\356\307\357\"\210\202P@G\316V\205@\360\203,\325\361@\"\202/\325\362!!\203J\211\2068\363\311\364\307!)\210\356\307\365#\266\202P\356\307\366\"\266\311\207@\205`\311\364\307@\311#)\311\207@\205`\311\211\367\307@\"\210\327\313\311#\210\320A@\211\205\206\370\"A\266\203\230\203\225\327\314\311#\210\202\233\327\314\307#\210*\311\207\371\307\323\324\325#!\266\311\327\313\307#)\311\207\311\372\307\323\324\325#!\266\211\340\330\211\307\373\"\211\211:\205\336\211@\342!\205\334\211G\343U\262\262\204\353\344\345\346\347E\"\210\266\202@D\")\311\207\360\325\374!!\203@\311\211\334\330\211\307\373\"\211\211:\205\211@\342!\205\211G\343U\262\262\204)\344\345\346\347E\"\210\266\202@D\"\266\327\313\307#)\210\356\307\365\"\210\202E\356\307\366\"\210\311\207\360\325\375!!\203\223\311\211\341\330\211\307\373\"\211\211:\205o\211@\342!\205m\211G\343U\262\262\204|\344\345\346\347E\"\210\266\202@D\"\266\327\313\307#)\210\356\307\365\"\210\202\230\356\307\366\"\210\311\207\360\325\376@\"!\203\265\311\377@\")\210\356\307\365\"\210\202\272\356\307\366\"\210\311\207\205`\201A =\203\312\313\"\204\310@G\316V\205\331@\211\203\357\201B @C\323\324\325#!\266\202\374\201C \307\323\324\325#!\266\211\206\363\311\364\307!)\266\202\310\311\201D \267\202\307\201E \307\323\324\325#!\266\211\334\330\211\307\373\"\211\211:\205B\211@\342!\205@\211G\343U\262\262\204O\344\345\346\347E\"\210\266\202@D\"\266\327\313\307#\210\202\307\201F \307\323\324\325#!\266\211\341\330\211\307\373\"\211\211:\205\213\211@\342!\205\211\211G\343U\262\262\204\230\344\345\346\347E\"\210\266\202@D\"\266\327\313\307#\210\202\307\201G @C\323\324\325#!\266\377@\"\210\202\307\377@\"\210)\307\211@\311\207\205`\201H \267\202\n\201I \307\323\324\325#!\266\202\n\201J \307\323\324\325#!\266\202\n\201K @C\323\324\325#!\266\307\211@\311\207\205`\201L \267\202L\201M \307\323\324\325#!\266\202L\201N \307\323\324\325#!\266\202L\201O @C\323\324\325#!\266\307\211@\311\207\201P \307\323\324\325#!\266\311\377\201Q \")\207@\203u\201R \202\210\312\312\314\"\203\204\201S \202\207\201T \"\211\316W\205\302\201U \307\323\324\325#!\266\201V C\f?\205\300\307@\211\203\277\211@\324@A$\262\210A\202\250\210\266\202\207\201W \307\323\324\325#!\266\211\337\330\211\307\373\"\211\211:\205\356\211@\342!\205\354\211G\343U\262\262\204\373\344\345\346\347E\"\210\266\202@D\"\266\327\313\307#\207\311\211\201X @D\f?\2052\307@\211\2031\211@\324@A$\262\210A\202\210)\207\311\211\201Y @D\f?\205]\307@\211\203\\\211@\324@A$\262\210A\202E\210)\207\307\207" [chess-engine-handling-event chess-engine-inhibit-auto-pass chess-engine-opponent-name chess-full-name chess-game-inhibit-events chess-engine-pending-offer chess-engine-game nil #s(hash-table size 18 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (move 9 pass 244 match 262 setup-pos 338 setup-game 354 quit 414 resign 435 draw 500 abort 583 undo 666 accept 700 decline 975 retract 1041 illegal 1107 call-flag 1130 flag-fell 1219 kibitz 1289 chat 1332)) t chess-game-data active my-color 3 0 chess-game-set-tag "White" "Black" now-black message apply chess-string pass chess-game-set-data chess-game-move last 2 chess-ply-any-keyword :drawn :perpetual :repetition :flag-fell :resign :aborted vectorp 75 signal wrong-type-argument chess-ply ply chess-pos position 74 :stalemate :checkmate move-passed chess-engine-command busy y-or-n-p want-to-play want-to-play-a "Anonymous" chess-engine-set-position accept decline chess-engine-set-game assoc opp-quit opp-resigned chess-game-ply opp-draw opp-abort opp-undo chess-game-undo chess-engine-pending-arg match opp-ready opp-ready-a #s(hash-table size 4 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (draw 791 abort 864 undo 937 my-undo 961)) opp-draw-acc opp-abort-acc opp-undo-acc #s(hash-table size 3 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (draw 987 abort 1003 undo 1019)) opp-draw-dec opp-abort-dec opp-undo-dec #s(hash-table size 3 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (draw 1053 abort 1069 undo 1085)) opp-draw-ret opp-abort-ret opp-undo-ret opp-illegal 1 -1 white-remaining black-remaining opp-call-flag flag-fell opp-flag-fell kibitz chat] 16 (#$ . 4017)])#@178 Create a new chess engine MODULE (a symbol) associated with GAME.
Optionally supply a new RESPONSE-HANDLER.

(fn MODULE GAME &optional RESPONSE-HANDLER &rest HANDLER-CTOR-ARGS)
(defalias 'chess-engine-create #[898 "\304\305\306%\211\205f r\211q\210\307\310\311!\312P!\313!\205 \211\262J!\206) \314\n\211\203[ \315!\203[ \316!\317>\204H \320\306\321\304\322#!\266\323!\203V \323!\324=\203[ \325\326\"\210\327 \330\331p#)\262\207" [chess-engine-regexp-alist chess-engine-response-handler chess-engine-process chess-engine-current-marker apply chess-module-create nil copy-alist intern-soft symbol-name "-regexp-alist" boundp chess-engine-default-handler processp process-status (run open listen) failed-start error chess-string process-filter internal-default-process-filter set-process-filter chess-engine-filter point-marker chess-game-set-data engine] 13 (#$ . 9291)])
(defalias 'chess-engine-destroy 'chess-module-destroy)#@90 Call the handler of ENGINE with EVENT (a symbol) and ARGS.

(fn ENGINE EVENT &rest ARGS)
(defalias 'chess-engine-command #[642 "\203 rq\210\302	$)\207\302	$\207" [chess-module-event-handler chess-module-game apply] 8 (#$ . 10243)])#@106 Set ENGINE OPTION to VALUE by invoking its handler with the 'set-option
event.

(fn ENGINE OPTION VALUE)
(defalias 'chess-engine-set-option #[771 "\203 rq\210\300\301$)\207\300\301$\207" [chess-engine-command set-option] 8 (#$ . 10490)])#@80 Set a new RESPONSE-HANDLER for ENGINE.

(fn ENGINE &optional RESPONSE-HANDLER)
(defalias 'chess-engine-set-response-handler #[513 "\203 rq\210\211\206 \301\211)\207\211\206 \301\211\207" [chess-engine-response-handler chess-engine-default-handler] 4 (#$ . 10742)])#@88 Return the function currently defined as the response-handler for ENGINE.

(fn ENGINE)
(defalias 'chess-engine-response-handler #[257 "\211\203 r\211q\210)\207\207" [chess-engine-response-handler] 2 (#$ . 11019)])#@43 

(fn ENGINE &optional POSITION MY-COLOR)
(defalias 'chess-engine-set-position #[769 "\203\347 rq\210\303\203f 	\211\304!\205 \211G\305U\262\204* \306\307\310\311E\"\210CCAA\2055 C\241\210\312D?\205[ \313@\211\203Z \211@\314@A$\262\210A\202C \210\266\315	\316#\210\202\273 	\n\211\304!\205u \211G\305U\262\204\202 \306\307\310\311E\"\210CCAA\205\215 C\241\210\312D?\205\263 \313@\211\203\262 \211@\314@A$\262\210A\202\233 \210\266\315	\316\303#\210\315	\317\303#)\210	\320C?\205\345 \313@\211\203\344 \211@\314@A$\262\210A\202\315 \210)\207\303\203E	\211\304!\205\374 \211G\305U\262\204	\306\307\310\311E\"\210CCAA\205C\241\210\312D?\205:\313@\211\2039\211@\314@A$\262\210A\202\"\210\266\315	\316#\210\202\232	\n\211\304!\205T\211G\305U\262\204a\306\307\310\311E\"\210CCAA\205lC\241\210\312D?\205\222\313@\211\203\221\211@\314@A$\262\210A\202z\210\266\315	\316\303#\210\315	\317\303#)\210	\320C?\205\304\313@\211\203\303\211@\314@A$\262\210A\202\254\210\207" [chess-game-inhibit-events chess-module-game chess-starting-position t vectorp 75 signal wrong-type-argument chess-pos position setup-game nil apply chess-game-set-data my-color active orient] 17 (#$ . 11241)])#@78 Return the current position of the game associated with ENGINE.

(fn ENGINE)
(defalias 'chess-engine-position #[257 "\211\2034 r\211q\210\301\302\"\211\211:\205\" \211@\303!\205  \211G\304U\262\262\204/ \305\306\307\310E\"\210\266\202@)\207\301\302\"\211\211:\205N \211@\303!\205L \211G\304U\262\262\204[ \305\306\307\310E\"\210@\207" [chess-module-game nil chess-game-ply vectorp 75 signal wrong-type-argument chess-ply ply] 9 (#$ . 12542)])
(byte-code "\300\301\302\"\210\300\303\304\"\210\300\305\306\"\210\300\307\310\"\207" [defalias chess-engine-game chess-module-game chess-engine-set-game chess-module-set-game chess-engine-set-game* chess-module-set-game* chess-engine-index chess-module-game-index] 3)#@19 

(fn ENGINE PLY)
(defalias 'chess-engine-move #[514 "\203 rq\210\301\"\210\302\303#)\207\301\"\210\302\303#\207" [chess-module-game chess-game-move chess-engine-command move] 6 (#$ . 13273)])
(chess-message-catalog 'english '((engine-not-running . "The engine you were using is no longer running")))#@250 Send the given STRING to ENGINE.
If `chess-engine-process' is a valid process object, use `process-send-string'
to submit the data.  Otherwise, the 'send event is triggered and the engine
event handler can take care of the data.

(fn ENGINE STRING)
(defalias 'chess-engine-send #[514 "\2035 rq\210\211\203. \301!\302>\203 \303\"\2023 \304\305\306\307\310#!\266\311\305\312\"\2023 \311\305\313#)\207\211\203W \301!\302>\203G \303\"\207\304\305\306\307\310#!\266\311\305\312\"\207\311\305\313#\207" [chess-engine-process process-status (run open) process-send-string engine-not-running nil message apply chess-string chess-engine-command destroy send] 10 (#$ . 13591)])#@85 Submit the given STRING, so ENGINE sees it in its input stream.

(fn ENGINE STRING)
(defalias 'chess-engine-submit #[514 "\2031 rq\210\211\203+ \301!\203+ \302!\303>\204+ \304\305\306\307\310#!\266\311\305\312\"\210\313\305\")\207\211\203T \301!\203T \302!\303>\204T \304\305\306\307\310#!\266\311\305\312\"\210\313\305\"\207" [chess-engine-process processp process-status (run open) engine-not-running nil message apply chess-string chess-engine-command destroy chess-engine-filter] 10 (#$ . 14283)])#@19 

(fn PROC EVENT)
(defalias 'chess-engine-sentinel #[514 "\300\301!!\207" [chess-engine-destroy process-buffer] 5 (#$ . 14806)])#@93 Filter for receiving text for an engine from an outside source.

(fn PROC &optional STRING)
(defalias 'chess-engine-filter #[513 "\203 \306!\203 \307!\202 p\310\311\312!\205\276 rq\210;\203* \262\202B `	U\212	b\210c\210	`\311\223)\210\211\203A 	b\210\210\n?\205\275 \310\212\203T b\210\202W eb\210\313\216m?\205\274 \211?\205\274 \311\f\311\211\203\247 \314@@\315 \310#\203\235 @A \211\262\203\235 \211\316=\203\227 \203\224 A\241\210\202\227 A\311\262\202i \262A\262\202i )\266\315 dU\203\266 \210\310\202Y \311y\210\202Y *))\207" [inhibit-redisplay chess-engine-current-marker chess-engine-working chess-engine-last-pos chess-engine-regexp-alist case-fold-search processp process-buffer t nil buffer-live-p #[0 "`\302\211\207" [chess-engine-last-pos chess-engine-working nil] 2] re-search-forward line-end-position once] 13 (#$ . 14941)])
(provide 'chess-engine)
