;; Generated package description from consult.el  -*- no-byte-compile: t -*-
(define-package "consult" "0.32" "Consulting completing-read" '((emacs "27.1") (compat "29.1.3.2")) :commit "0759dd12bb2290480644a7ec34118f67718074ad" :authors '(("Daniel Mendler and Consult contributors")) :maintainer '("Daniel Mendler" . "mail@daniel-mendler.de") :url "https://github.com/minad/consult")
