;; Generated package description from php-mode.el  -*- no-byte-compile: t -*-
(define-package "php-mode" "1.24.2" "Major mode for editing PHP code" '((emacs "25.2")) :commit "d63b8af8333b53ef05b1f9b9e066253497302a50" :authors '(("Eric James Michael Ritz")) :maintainer '("USAMI Kenta" . "tadsan@zonu.me") :keywords '("languages" "php") :url "https://github.com/emacs-php/php-mode")
