;; Generated package description from nasm-mode.el  -*- no-byte-compile: t -*-
(define-package "nasm-mode" "1.1.1" "NASM x86 assembly major mode" '((emacs "24.3")) :authors '(("Christopher Wellons" . "wellons@nullprogram.com")) :maintainer '("Christopher Wellons" . "wellons@nullprogram.com") :url "https://github.com/skeeto/nasm-mode")
