(define-package "dashboard" "20230216.2155" "A startup screen extracted from Spacemacs"
  '((emacs "26.1"))
  :commit "ec61c1b469cc45e15f92672825f653aa308ddb1b" :authors
  '(("Rakan Al-Hneiti" . "rakan.alhneiti@gmail.com"))
  :maintainer
  '("Jesús Martínez" . "jesusmartinez93@gmail.com")
  :keywords
  '("startup" "screen" "tools" "dashboard")
  :url "https://github.com/emacs-dashboard/emacs-dashboard")
;; Local Variables:
;; no-byte-compile: t
;; End:
